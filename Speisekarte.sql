create database Speisekarte;
use Speisekarte;


	create table Gericht
	(
	gId int auto_increment primary key,
	name varchar(50)  not null,
	preis varchar(10) not null
	);
 
	create table Zutat
	(
	zId int auto_increment primary key,
	name varchar(50) not null
	);
  
	create table Drink
	(
	dId int auto_increment primary key,
	name varchar(50)  not null,
	preis varchar(10) not null,
	size varchar(50) not null
	);
