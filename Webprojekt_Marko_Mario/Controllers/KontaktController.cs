﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webprojekt_Marko_Mario.Models;
using Webprojekt_Marko_Mario.Models.db;
using MySql.Data.MySqlClient;


namespace Webprojekt_Marko_Mario.Controllers
{
    public class KontaktController : Controller
    {
        // GET: Kontakt
        public ActionResult Index()
        {
            return View();
        }

        //HttpGet wird aufgerufen falls auf einen link geklickt wirrd
        [HttpGet]
        public ActionResult Kontakt()
        {
            Kontakt k = new Kontakt();
            return View(k);
        }

        //Schnittstelle verwenden
        IKontaktRepository repKontakt;

        //hhtppotst wird aufgerufen wenn das formular mit der methode POST abgesendet wird
        [HttpPost]
        public ActionResult Kontakt(Kontakt kontaktDataFromForm)
        {

            //Parameter überprüfen
            if (kontaktDataFromForm != null)
            {
                //Formulardatem überprüfen
                ValidationForKontaktForm(kontaktDataFromForm);

                if (ModelState.IsValid)
                {
                    repKontakt = new KontaktRepositoryDB();
                    repKontakt.Open();
                    try
                    {
                        if (repKontakt.Insert(kontaktDataFromForm))
                        {
                            return View("Message", new Message("Erfolgreich gesendet", "Vielen Dank", "Wir werden ihnen so schnell wie möglich antworten",""));               
                        }
                        else
                        {  
                            return View("Message", new Message("Fehler", "", "Etwas ist schiefgelaufen", "versuchen sie es später erneut"));
                        }

                    }
                    catch (MySqlException)
                    {
                        return View("Message", new Message("Datenbankfehler", "", "Etwas ist schiefgelaufen", "versuchen sie es später erneut"));

                    }
                    catch (Exception)
                    {
                        return View("Message", new Message("Fehler", "", "Etwas ist schiefgelaufen", "versuchen sie es später erneut"));
                    }
                    finally
                    {
                        repKontakt.Close();
                    }
                }
                else
                {
                    //Bei einem Fehler wird das Kontakt Formular aufgerufen
                    //dabei werden die eingegebenen Daten an das Formular übergeben
                    return View(kontaktDataFromForm);
                }
            }
            else
            {
                return RedirectToAction("Kontakt");
            }

        }
        private void ValidationForKontaktForm(Kontakt k)
        {
            //Reihenfolge beachten 
            if ((k.Name == null) || (k.Name.Trim().Length > 50))
            {
                //name ..wie das Property in der Klasse registered person lautet
                ModelState.AddModelError("Name", "Es wurde kein Name angegeben oder er ist zu lange");
                //diese Zeile würde auch das Feld ModelState.Ivalid auf false setzen
            }
            if (k.EMail == null) 
            {
                ModelState.AddModelError("EMail", "Die Email Adresse ist ein Pflichtfeld");
            }

            if ((k.Betreff == null) || (k.Betreff.Length > 15)) // || (person.Password.IndexOfAny(new[] { '!', '$', '%', '/', '§'}) == -1)) 
            {
                ModelState.AddModelError("Betreff", "Es wurde kein Betreff eingegeben oder er ist zu lang");

            }
            if (k.Nachricht == null) 
            {
                ModelState.AddModelError("Nachricht", "Es wurde keine Nachricht eingegeben");

            }
        }
    }
}