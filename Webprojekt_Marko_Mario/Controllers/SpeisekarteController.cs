﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Webprojekt_Marko_Mario.Models;
using Webprojekt_Marko_Mario.Models.db;


namespace Webprojekt_Marko_Mario.Controllers
{
    public class SpeisekarteController : Controller
    {
        // GET: Speisekarte
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Menü()
        {
            return View();
        }

        //OHNE DATENBANK
        public ActionResult Drink()
        {
            List<Drink> drinks = GetDrinksList();
            return View(drinks);
        }

        public ActionResult Gericht()
        {
            List<Gericht> gerichte = GetGerichtList();
            return View(gerichte);
        }


        private List<Drink> GetDrinksList()
        {

            return new List<Drink>() {
                new Drink(14, "Bier", null, 2, "0,25l"),
                 new Drink(14, "Bier", null, 4, "0,50l"),
                new Drink(0.0, "Wasser", null, 1, "0,25l"),
                new Drink(0.0, "Saft",null,2,"0,25l")
            };
        }


        public List<string> DiavoloList = new List<string> { "Tomaten", "Käse", "Salami" };
        public List<string> MargheritaList = new List<string> { "Tomaten", "Käse", "Oregano" };
        public List<string> FunghiList = new List<string> { "Tomaten", "Käse", "Champignon" };


        private List<Gericht> GetGerichtList()
        {

            return new List<Gericht>() {
                new Gericht(Category.scharf, Size.medium, "Diavolo", DiavoloList, 8),
                new Gericht(Category.mild, Size.xl, "Margherita ", MargheritaList,7),
                new Gericht(Category.mild, Size.xl, "Funghi ", FunghiList,7),

            };

        }

        //MIT DATENBANK

        //Interface verwenden
        ISpeisekarteRepository gerichteRepository;
        ISpeisekarteRepository drinksRepository;


        public ActionResult GerichtDatenbank(Gericht gericht)
        {

            // diese Methode soll alle Gerichte an eine View übergeben
            //  und dort in Tabellenform anzweigen
            try
            {
                gerichteRepository = new SpeisekarteRepositoryDB();
                gerichteRepository.Open();

                return View(gerichteRepository.GetAllGerichte());
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
            }
            finally
            {
                gerichteRepository.Close();
            }
        }



        public ActionResult DrinkDatenbank(Drink drinks)
        {

            // diese Methode soll alle Gerichte an eine View übergeben
            //  und dort in Tabellenform anzweigen
            try
            {
                drinksRepository = new SpeisekarteRepositoryDB();
                drinksRepository.Open();

                return View(drinksRepository.GetAllDrinks());
            }
            catch (MySqlException)
            {
                return View("Message", new Message("Datenbankfehler", "", "Probleme mit der Datenbank.", "Versuchen Sie es später erneut."));
            }
            finally
            {
                drinksRepository.Close();
            }
        }

    }
}
