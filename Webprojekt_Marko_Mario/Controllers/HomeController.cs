﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Webprojekt_Marko_Mario.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Kontakt()
        {
            return View();
        }
        public ActionResult Reservieren()
        {
            return View();
        }
        public ActionResult Wir()
        {
            return View();
        }
        public ActionResult Menue()
        {
            return View();
        }
    }
}