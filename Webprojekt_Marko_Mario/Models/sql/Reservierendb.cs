﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models.sql
{
    public class Reservierendb

    { 
        private string _connectionString = "Server=localhost;Database=Reservieren_Datenbank;Uid=root;Pwd=12345";
        private MySqlConnection _connection;

        public void Open()
        {
            try
            {
                if (this._connection == null)
                {
                    this._connection = new MySqlConnection(this._connectionString);
                }
                if (this._connection.State != ConnectionState.Open)
                {
                    this._connection.Open();
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public void Close()
        {

            try
            {
                if (this._connection != null && this._connection.State == ConnectionState.Open)
                {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
    }
    }