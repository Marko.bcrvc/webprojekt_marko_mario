﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{

    public class Drink : Speisekarte
    {
        public double _alkoholgehalt;
        public string Size;

        public double Alkoholgehalt
        {
            get { return this._alkoholgehalt; }
            set
            {
                if (value >= 0.0)
                {
                    this._alkoholgehalt = value;
                }
            }
        }   

        public Drink() : this(0.0, "", null, 0.0m, "") { }
        public Drink(double alkoholgehalt, string name, List<string> zutaten, decimal preis, string size) : base(name, zutaten, preis)
        {
            this.Alkoholgehalt = alkoholgehalt;
            this.Size = size;
        }
        public override string ToString()
        {
            return this.Alkoholgehalt + Environment.NewLine;
        }

    }
}