﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{
  
        public class User
        {

            public int ID { get; set; }
            public string Firstname { get; set; }
            public string Lastname { get; set; }
            public string Password { get; set; }
            public string EMail { get; set; }
            public DateTime? Birthdate { get; set; }
            public string Address { get; set; }
            public int Postcode { get; set; }
            public string City { get; set; }

            public User() : this("", "", "", "", "", null, "", 0, "") { }
            public User(string firstname, string lastname, string pwd, string eMail, string username, DateTime? birthdate, string address, int postcode, string city)
            {
                this.ID = -1;
                this.Firstname = firstname;
                this.Lastname = lastname;
                this.Password = pwd;
                this.EMail = eMail;
                this.Birthdate = birthdate;
                this.Address = address;
                this.Postcode = postcode;
                this.City = city;

            }

            // ToString()
        }
    }

