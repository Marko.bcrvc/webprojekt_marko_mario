﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{
    public class Kontakt
    {

        public string Name { get; set; }
        public string EMail { get; set; }
        public string Betreff { get; set; }
        public string Nachricht { get; set; }

        public Kontakt() : this("", "", "", "") { }
        public Kontakt(string name, string eMail, string betreff, string nachricht)
        {
            this.Name = name;
            this.EMail = eMail;
            this.Betreff = betreff;
            this.Nachricht = nachricht;

        }

        // ToString()
    }
}