﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;

namespace Webprojekt_Marko_Mario.Models.db
{
    interface ISpeisekarteRepository
    {

        void Open();
        void Close();

        List<Gericht> GetAllGerichte();
        List<Drink> GetAllDrinks();
        bool ChangeGerichteData(int gerichtIdToChange, Gericht newGerichtData);
        bool ChangeDrinkData(int drinkIdToChange, Drink newDrinkData);


    }
}
