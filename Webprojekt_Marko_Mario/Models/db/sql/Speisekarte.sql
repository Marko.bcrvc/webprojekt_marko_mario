
use Speisekarte;


	create table Gericht
	(
	gId int auto_increment primary key,
	name varchar(50)  not null,
	preis varchar(10) not null
	)engine=InnoDB;
 
	create table Zutat
	(
	zId int auto_increment primary key,
	name varchar(50) not null
	)engine=InnoDB;
  
	create table Drink
	(
	dId int auto_increment primary key,
	name varchar(50)  not null,
	preis varchar(10) not null,
	size varchar(50) not null
	)engine=InnoDB;

show fields from Gericht;

use Speisekarte;
INSERT INTO Drink (name, preis, size) VALUES ('Cola', '8', '0,5');
INSERT INTO Drink (name, preis, size) VALUES ('Sprite', '5', '0,5');
INSERT INTO Drink (name, preis, size) VALUES ('Fanta', '5', '0,5');
INSERT INTO Zutat (zId, name) VALUES('01', 'Tomate');
INSERT INTO Zutat (zId, name) VALUES('02', 'Kaese');
INSERT INTO Gericht (gId, name, preis) VALUES('01', 'Diavolo', '8');

