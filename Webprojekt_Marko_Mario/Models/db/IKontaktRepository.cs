﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;

namespace Webprojekt_Marko_Mario.Models.db
{
    interface IKontaktRepository
    {

        void Open();
        void Close();

        bool Insert(Kontakt kontaktToInsert);
    }
}
