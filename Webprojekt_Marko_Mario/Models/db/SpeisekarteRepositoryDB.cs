﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;


namespace Webprojekt_Marko_Mario.Models.db
{
    public class SpeisekarteRepositoryDB : ISpeisekarteRepository
    {
        private string _connectionString = "Server=localhost; Database=Speisekarte; Uid=root; SslMode=none";
        private MySqlConnection _connection;

        public bool ChangeDrinkData(int drinkIdToChange, Drink newDrinkData)
        {
            throw new NotImplementedException();
        }

        public bool ChangeGerichteData(int gerichtIdToChange, Gericht newGerichtData)
        {
            throw new NotImplementedException();
        }


        public void Open()
        {
            try
            {
                if (this._connection == null)
                {
                    this._connection = new MySqlConnection(this._connectionString);
                }
                if (this._connection.State != ConnectionState.Open)
                {
                    this._connection.Open();
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public void Close()
        {

            try
            {
                if (this._connection != null && this._connection.State == ConnectionState.Open)
                {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public List<Drink> GetAllDrinks()
        {

            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            List<Drink> allDrinks = new List<Drink>();
            try
            {
                MySqlCommand cmdSelectAllDrinks = this._connection.CreateCommand();
                cmdSelectAllDrinks.CommandText = "SELECT * FROM Drink";

                // ExecuteReader() wird bei Select-Abfragen verwendet
                using (MySqlDataReader reader = cmdSelectAllDrinks.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        // Read() liefert true/false zurück
                        //  true ... weitere Datensätze sind vorhanden
                        //  false ... kein Datensatz mehr vorhanden
                        while (reader.Read())
                        {

                            allDrinks.Add(
                            new Drink
                            {
                                // Name ... Feldname in der Klasse 
                                // "name" ... Name des Feldes in der DB-Tabelle
                                Name = Convert.ToString(reader["name"]),
                                Preis = Convert.ToDecimal(reader["preis"]),
                                Size = Convert.ToString(reader["size"])//(Size) Convert.ToInt32(reader["size"])
                            }
                                    );
                        }
                    }
                }

            }
            catch (MySqlException)
            {
                throw;
            }

            return allDrinks.Count >= 1 ? allDrinks : null;
        }

        





        public List<string> GetAllZutaten()
        {

            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            List<string> alleZutaten = new List<string>();
            try
            {
                MySqlCommand cmdSelectAlleZutaten = this._connection.CreateCommand();
                cmdSelectAlleZutaten.CommandText = "SELECT * FROM Zutat";
              

                using (MySqlDataReader reader = cmdSelectAlleZutaten.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            alleZutaten.Add(Convert.ToString(reader["name"]));
                        }
                    }
                }

            }
            catch (MySqlException)
            {
                throw;
            }
            return alleZutaten.Count >= 1 ? alleZutaten : null;
        }

        public List<Gericht> GetAllGerichte()
        {

            if (this._connection == null || this._connection.State != ConnectionState.Open)
            {
                return null;
            }

            List<Gericht> alleGerichte = new List<Gericht>();
            try
            {
                MySqlCommand cmdSelectAlleGerichte = this._connection.CreateCommand();
                cmdSelectAlleGerichte.CommandText = "SELECT * FROM Gericht";


                using (MySqlDataReader reader = cmdSelectAlleGerichte.ExecuteReader())
                {
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {

                            alleGerichte.Add(
                            new Gericht
                            {

                                Name = Convert.ToString(reader["name"]),
                                Preis = Convert.ToDecimal(reader["preis"]),
                                Zutaten = GetAllZutaten()
                            }
                                            );
                        }
                    }
                }

            }
            catch (MySqlException)
            {
                throw;
            }
            return alleGerichte.Count >= 1 ? alleGerichte : null;
        }
    }
}
