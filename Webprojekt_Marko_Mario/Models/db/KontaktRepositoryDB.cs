﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models.db
{
    public class KontaktRepositoryDB : IKontaktRepository
    {
        private string _connectionString = "Server=localhost; Database=Kontakt; Uid=root; SslMode=none";
        private MySqlConnection _connection;



        public void Open()
        {
            try
            {
                if (this._connection == null)
                {
                    this._connection = new MySqlConnection(this._connectionString);
                }
                if (this._connection.State != ConnectionState.Open)
                {
                    this._connection.Open();
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public void Close()
        {

            try
            {
                if (this._connection != null && this._connection.State == ConnectionState.Open)
                {
                    this._connection.Close();
                    this._connection = null;
                }
            }
            catch (MySqlException)
            {
                throw;
            }

        }
        public bool Insert(Kontakt kontaktToInsert)
        {
            if (kontaktToInsert == null)
            {
                return false;
            }

            try
            {
                //Command (Befehl) erzeugen
                MySqlCommand cmdInsert = this._connection.CreateCommand();
                //bei CommandText wird der SQL-Befehl angeben
                //@name ...name kann beliebig gewählt werden
                cmdInsert.CommandText = "insert into kontakt values(@name, @email, @betreff, @nachricht)";
                cmdInsert.Parameters.AddWithValue("name", kontaktToInsert.Name);
                cmdInsert.Parameters.AddWithValue("email", kontaktToInsert.EMail);
                cmdInsert.Parameters.AddWithValue("betreff", kontaktToInsert.Betreff);
                cmdInsert.Parameters.AddWithValue("nachricht", kontaktToInsert.Nachricht);
   
                //ExecuteNonQuery() wird bei allen SQL-Befehlen verwendet außer bei SELECT
                //ExecuteNonQuery() gitb die Anzahl der betroffenen Datensätze zurück 
                return cmdInsert.ExecuteNonQuery() == 1;
                //Ist die Basis Klasse für alle weiteren MySql - Exceptions
                //Wir verwenden Exception, da bei this._connection.CreateCommand()
                //       _connection null sein könnte => keine datenbankspeziefische
                //       Exception
            }
            catch (Exception)
            {
                //Die erzeugte Exception wird erneut ausgelöst 
                throw;
            }
        }
    }
}