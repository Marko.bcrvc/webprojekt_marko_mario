﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{
    public class Speisekarte
    {
        public string Name { get; set; }
        public List<string> Zutaten = new List<string>();
        private decimal _preis;


        public decimal Preis
        {
            get { return this._preis; }
            set
            {
                if (value >= 0.0m)
                {
                    this._preis = value;
                }
            }

        }
        public Speisekarte(): this("", null, 0.0m) { }
        public Speisekarte(string name, List<string> zutaten, decimal preis)
        {
            this.Name = name;
            this.Zutaten = zutaten;
            this.Preis = preis;
        }
        public override string ToString()
        {
            return "Name: " + this.Name + Environment.NewLine + "Zutaten: " + this.Zutaten + Environment.NewLine + "Preis: " + this.Preis + " Euro";
        } 

    }
}