﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{
    public enum Category { scharf, mild, notSpecified }
    public enum Size { small, medium, xl, notSpecified }

     public class Gericht : Speisekarte
    {
        public Category Category { get; set; }
        public Size Size { get; set; }

        public Gericht() : this(Category.notSpecified, Size.notSpecified, "", null, 0.0m) { }
        public Gericht(Category category, Size size, string name, List<string> zutaten, decimal preis) : base(name, zutaten, preis)
        {
            this.Category = category;
            this.Size = size;
        }
        public override string ToString()
        {
            return this.Name + " " + this.Category + " " + this.Preis;

        }
    }
}