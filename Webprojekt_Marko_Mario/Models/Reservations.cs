﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Webprojekt_Marko_Mario.Models
{
    public enum Smoking
    {
        Raucher, Nichtraucher,
    }
    public class Reservations
    {
        public string Lastname { get; set; }
        public DateTime? Datum { get; set; }
        public int PersonNumber { get; set; }
        public int PhoneNumber { get; set; }
        public Smoking Smoking { get; set; }

        public Reservations() : this("",null,0,0,Smoking.Nichtraucher) { }
        public Reservations(string lastname, DateTime? datum,int personNumber, int phoneNumber, Smoking smoking )
        {
            this.Lastname = lastname;
            this.Datum = datum;
            this.PersonNumber = personNumber;
            this.PhoneNumber = phoneNumber;
            this.Smoking = smoking;
        }

    }
}